package hw6;

/*
 * Class Pet provides ability to create instances of Pet. It contains 6 private fields, 7 final Strings to negotiate via Console, 6 methods, 2
 * constructors, and 4 overrided methods. describeTrickLevel() returns final String (one of two specials) depending on trickLevel
 * passed via constructor, overrided hashCode() and equals(), toString() and finalize()
 *
 * @version  3.1 5 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import java.util.Arrays;

public class Pet {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String [] habits;

    final String EAT = "I'm eating";
    final String RESPOND = "Hello, master. I'm %s. I missed you!!! \n";
    final String FOUL = "I need to cover my tracks well...";
    final String TRICKY = "very tricky";
    final String NOT_TRICKY = "almost not tricky";
    final String ANNOUNCEMENT = "New Object had been created of type Pet!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";

    public Species getSpecies () {
        return species;
    }
    public String getNickname () {
        return nickname;
    }
    public int getAge () {
        return age;
    }
    public int getTrickLevel () {
        return trickLevel;
    }
    public String [] getHabits () {
        return habits;
    }

    public void setSpecies (Species species) {
        this.species = species;
    }
    public void setNickname (String nickname) {
        this.nickname = nickname;
    }
    public void setAge (int age) {
        this.age = age;
    }
    public void setTrickLevel (int level) {
        this.trickLevel = level;
    }
    public void setHabits (String [] args) {
        this.habits = args;
    }

    void eat () {
        System.out.println(EAT);
    }

    void respond () {
        System.out.printf(RESPOND, getNickname());
    }

    void foul () {
        System.out.println(FOUL);
    }

    public String describeTrickLevel () {
        if (getTrickLevel() > 50) {
            return TRICKY;
        } else {
            return NOT_TRICKY;
        }
    }

    public boolean isTimeToEat (int randomZeroOrOne , int random) {
        if (randomZeroOrOne == 0) {
            return trickLevel < random;
        } else {
            return true;
        }
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        System.out.println(ANNOUNCEMENT);
    }

    public Pet (Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this(species, nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    public String toString() {

        return getSpecies() + " { nickname = "+getNickname() +", age = "+getAge()+", trickLevel = "+getTrickLevel() +", habits = "
                + Arrays.toString(getHabits()) +" }";
    }
    @Override
    public int hashCode () {
        return species.name().length() * 2 + nickname.length() * 2;
    }

    @Override
    public boolean equals (Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Pet)) {
            return false;
        }
        return this.hashCode() == obj.hashCode() && this.species.equals(((Pet) obj).species)
                && this.nickname.equals(((Pet) obj).nickname);
    }
    @Override
    public void finalize () {
        System.out.printf("Deleted obj: %s, has hashCode: %d, of species: %s and nickname: %s",
                this.getClass().getName(), this.hashCode(), this.species.name(), this.nickname);
    }
}