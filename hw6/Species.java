package hw6;

/*
* Simple enum Species
*
* */

public enum Species {
    CAT,
    DOG,
    SNAKE,
    RAT,
    PARROT,
    RABBIT,
    SQUIRREL,
    HORSE
}