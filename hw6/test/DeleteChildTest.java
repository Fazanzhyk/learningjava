package hw6.test;

import hw6.Family;
import hw6.Human;
import hw6.Pet;
import hw6.Species;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DeleteChildTest {

    @Test
    public void deleteChildByIndexShouldReturnTrue () throws Exception {
        Human tony = new Human ("Tony" , "Chekhov", 1989);
        Human bianka = new Human("Bianka", "Dedovic", 1999);
        Human tracy = new Human ("Trace", "Chekhov", 2019);
        Human billy = new Human ("Billy", "Dedovic", 2020);
        Pet bob = new Pet (Species.PARROT, "Bob");
        Family family = new Family(bianka, tony, bob, tracy, billy);
        assertTrue(family.deleteChild(0));
    }

    @Test
    public void deleteChildByIndexShouldNotDeleteChildAndReturnFalse () throws Exception {
        Human tony = new Human ("Tony" , "Chekhov", 1989);
        Human bianka = new Human("Bianka", "Dedovic", 1999);
        Human tracy = new Human ("Trace", "Chekhov", 2019);
        Human billy = new Human ("Billy", "Dedovic", 2020);
        Pet bob = new Pet (Species.PARROT, "Bob");
        Family family = new Family(bianka, tony, bob, tracy, billy);
        assertFalse(family.deleteChild(3));
    }

    @Test
    public void deleteChildByHumanShouldReturnTrue () throws Exception {
        Human tony = new Human ("Tony" , "Chekhov", 1989);
        Human bianka = new Human("Bianka", "Dedovic", 1999);
        Human tracy = new Human ("Trace", "Chekhov", 2019);
        Human billy = new Human ("Billy", "Dedovic", 2020);
        Pet bob = new Pet (Species.PARROT, "Bob");
        Family family = new Family(bianka, tony, bob, tracy, billy);
        assertTrue(family.deleteChild(billy));
    }

    @Test
    public void deleteChildByHumanShouldNotDeleteChildAndReturnFalse () throws Exception {
        Human tony = new Human ("Tony" , "Chekhov", 1989);
        Human bianka = new Human("Bianka", "Dedovic", 1999);
        Human tracy = new Human ("Trace", "Chekhov", 2019);
        Human billy = new Human ("Billy", "Dedovic", 2020);
        Pet bob = new Pet (Species.PARROT, "Bob");
        Human jonny = new Human ("Jonny", "Belucci", 1949);
        Family family = new Family(bianka, tony, bob, tracy, billy);
        assertFalse(family.deleteChild(jonny));
    }


}
