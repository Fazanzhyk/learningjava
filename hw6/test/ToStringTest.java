package hw6.test;

import hw6.*;
import org.junit.Test;

public class ToStringTest {

    @Test
    public void twoStringsForPetShouldBeEquals() throws Exception {
        Pet pet = new Pet (Species.DOG, "Sharik", 5, 56, new String[]{"eat", "sleep", "barkle"});
        String str = pet.toString();
        String str1 = "DOG { nickname = Sharik, age = 5, trickLevel = 56, habits = [eat, sleep, barkle] }";
        assert str.equals(str1);
    }

    @Test
    public void twoStringsForHumanShouldBeEquals () throws Exception {
        String [][] schedule = new String [7][2];
        schedule[0][0] = DayOfWeek.MONDAY.name();
        schedule[0][1] = "Go to DAN IT; Do HW3";
        schedule[1][0] = DayOfWeek.TUESDAY.name();
        schedule[1][1] = "Lay on couch; eat some burgers";
        schedule[2][0] = DayOfWeek.WEDNESDAY.name();
        schedule[2][1] = "Go to DAN IT; DO HW4";
        schedule[3][0] = DayOfWeek.THURSDAY.name();
        schedule[3][1] = "Go to cinema; drink some beer";
        schedule[4][0] = DayOfWeek.FRIDAY.name();
        schedule[4][1] = "Go to dance club; drink some vodka";
        schedule[5][0] = DayOfWeek.SATURDAY.name();
        schedule[5][1] = "Go to DAN IT to work on ClassLoader, etc.";
        schedule[6][0] = DayOfWeek.SUNDAY.name();
        schedule[6][1] = "Watch movie, play on guitar, do my lazy stuff";

        Human human = new Human ("Igor", "Ivanov", 1989, 200, schedule);
        String str = human.toString();
        String str2 = "Human { name = Igor, surname = Ivanov, year = 1989, iq = 200, schedule = [[MONDAY, Go to DAN IT; Do HW3], " +
                "[TUESDAY, Lay on couch; eat some burgers], [WEDNESDAY, Go to DAN IT; DO HW4], [THURSDAY, Go to cinema; drink some beer], " +
                "[FRIDAY, Go to dance club; drink some vodka], [SATURDAY, Go to DAN IT to work on ClassLoader, etc.], " +
                "[SUNDAY, Watch movie, play on guitar, do my lazy stuff]]";
        assert str.equals(str2);
    }

    @Test
    public void twoStringsForFamilyShouldBeEquals () throws Exception {
        Human tony = new Human ("Tony" , "Chekhov", 1989);
        Human bianka = new Human("Bianka", "Dedovic", 1999);
        Human tracy = new Human ("Trace", "Chekhov", 2019);
        Human billy = new Human ("Billy", "Dedovic", 2020);
        Pet bob = new Pet (Species.PARROT, "Bob");
        Family family = new Family(bianka, tony, bob, tracy, billy);
        String str = family.toString();
        String str2 = "Mother: Human { name = Bianka, surname = Dedovic, year = 1999, iq = 0, schedule = null " +
                "Father: Human { name = Tony, surname = Chekhov, year = 1989, iq = 0, schedule = null " +
                "Children: [Human { name = Trace, surname = Chekhov, year = 2019, iq = 0, schedule = null, " +
                "Human { name = Billy, surname = Dedovic, year = 2020, iq = 0, schedule = null] " +
                "Pet: PARROT { nickname = Bob, age = 0, trickLevel = 0, habits = null }";

        assert str.equals(str2);
    }
}
