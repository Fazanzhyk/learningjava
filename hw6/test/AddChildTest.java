package hw6.test;

import hw6.Family;
import hw6.Human;
import hw6.Pet;
import hw6.Species;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AddChildTest {

    @Test
    public void testAddChildShouldReturnTrueIfArrayGrownByOne () throws Exception {

        Human tony = new Human ("Tony" , "Chekhov", 1989);
        Human bianka = new Human("Bianka", "Dedovic", 1999);
        Human tracy = new Human ("Trace", "Chekhov", 2019);
        Human billy = new Human ("Billy", "Dedovic", 2020);
        Pet bob = new Pet (Species.PARROT, "Bob");
        Family family = new Family(bianka, tony, bob, tracy);
        Human [] child = family.getChildren();
        family.addChild(billy);
        Human [] child1 = family.getChildren();
        assertEquals(1, child1.length - child.length);
        assertEquals(billy, child1[child1.length - 1]);
    }

}
