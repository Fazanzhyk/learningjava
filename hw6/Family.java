package hw6;

/*
 * Class Family provides ability to create instances of Family. It contains 5 private fields, 2 final Strings to negotiate via Console,
 * 3 methods (add/deleteChild & countFamily()), 1 constructor, and 4 overrided methods.
 *
 * @version  2.1  5 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import java.util.Arrays;

public class Family {

    private Human mother;
    private Human father;
    private Human [] children;
    private Pet pet;

    final String ANNOUNCEMENT = "New Object had been created of type Family!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";

    public Human getMother () {
        return mother;
    }
    public Human getFather () {
        return father;
    }
    public Human [] getChildren () {
        return children;
    }
    public Pet getPet () {
        return pet;
    }

    public void setMother (Human mother) {
        this.mother = mother;
    }
    public void setFather (Human father) {
        this.father = father;
    }
    public void setChildren (Human [] args) {
        this.children = args;
    }
    public void setPet (Pet pet) {
        this.pet = pet;
    }

    public void addChild (Human human) {
        int length = children.length + 1;
        Human [] arr = new Human [length];
        for (int i = 0; i < children.length; i++) {
            arr[i] = children[i];
        }
        for (int j = 0; j < arr.length; j++) {
            if (arr[j] == null) {
                arr[j] = human;
            }
        }
        human.setFamily(this);
        setChildren(arr);
    }

    public boolean deleteChild (int index) {
        int bef = children.length;
        int length = bef - 1;
        Human [] arr = new Human [length];
        if (index <= length) {
            children[index].setFamily(null);
            if (!(index == 0 && length == 1)) {
                for (int i = 0; i < index; i++) {
                    arr[i] = children[i];
                }
                for (int i = length; i > index; i--) {
                    arr[i] = children[i];
                }
            } else {
                arr[0] = children [1];
            }
            setChildren(arr);
        } else {
            setChildren(children);
        }
        int aft = children.length;

        return bef != aft;
    }

    public boolean deleteChild (Human human) {
        int bef = children.length;
        Human [] arr = new Human[children.length - 1];
        int index = 0;
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(human)) {
                index = i;
                break;
            } else {
                index = -1;
            }
        }
        if (index != -1) {
            if (!(index == 0 && arr.length == 1)) {
                for (int i = 0; i < index; i++) {
                    arr[i] = children[i];
                }
                for (int i = arr.length; i > index; i--) {
                    arr[i] = children[i];
                }
            } else {
                arr[0] = children [1];
            }
            children[index].setFamily(null);
            setChildren(arr);
        } else {
            setChildren(children);
        }
        int aft = children.length;

        return bef != aft;
    }

    public int countFamily () {
        return 2 + children.length;
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        System.out.println(ANNOUNCEMENT);
    }

    public Family (Human mother, Human father, Pet pet, Human ...args) {
            setMother(mother);
            setFather(father);
            setPet(pet);
            setChildren(args);
            mother.setFamily(this);
            father.setFamily(this);
        for (Human arg : args) {
            arg.setFamily(this);
        }
    }

    @Override
    public String toString() {
        return "Mother: "+mother.toString()+" Father: "+father.toString()+" Children: "+ Arrays.toString(children) +" Pet: " + pet.toString();
    }

    @Override
    public int hashCode() {
        int result = mother.hashCode() + father.hashCode() + pet.hashCode();
        for (Human child : children) {
            result += child.hashCode();
        }
        return result;
    }

    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Family)) {
            return false;
        }
        return this.hashCode() == obj.hashCode() && this.mother==((Family) obj).mother
                && this.father==((Family) obj).father;
    }

    @Override
    public void finalize () {
        System.out.printf("Deleted obj: %s, has hashCode: %d", this.getClass().getName(), this.hashCode());
    }
}