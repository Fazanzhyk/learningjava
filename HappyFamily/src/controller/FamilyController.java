package happyfamily.controller;

/*
* Class FamilyController extends FamilyService and using to create instances to manage with familyList
*
* @version  4.0  02 Mar 2020
*
* @author   Igor Ivanov
**/

import happyfamily.FamilyOverflowException;
import happyfamily.family.Family;
import happyfamily.human.Human;
import happyfamily.pets.Pet;
import happyfamily.sevice.FamilyService;

import java.text.ParseException;
import java.util.List;
import java.util.Set;

public class FamilyController {

    private FamilyService familyService;

    public FamilyService getFamilyService() {
        return familyService;
    }

    public FamilyController () {
        this.familyService = new FamilyService();
    }

    final String FAMILY_EXCEPTION_MESSAGE = "This family already has maximum persons: 5";

    public void displayAllFamilies () {
        familyService.displayAllFamilies();
    }

    public List<Family> getFamiliesBiggerThan(int number) {
       return familyService.getFamiliesBiggerThan(number);
    }

    public List<Family> getFamiliesLessThan(int number) {
        return familyService.getFamiliesLessThan(number);
    }

    public long countFamiliesWithMemberNumber(int number) {
        return familyService.countFamiliesWithMemberNumber(number);
    }

    public void createNewFamily(Human mother, Human father) {
        familyService.createNewFamily(mother, father);
    }

    public Family bornChild(Family family, String maleName, String femaleName) throws ParseException, FamilyOverflowException {
            return familyService.bornChild(family, maleName, femaleName);
    }

    public Family adoptChild(Family family, Human human) throws FamilyOverflowException {
        return familyService.adoptChild(family, human);
    }

    public void deleteAllChildrenOlderThen(int childrenAge) {
        familyService.deleteAllChildrenOlderThen(childrenAge);
    }

    public int count() {
        return familyService.count();
    }

    public Set<Pet> getPets(int familyIndex) {
        return familyService.getPets(familyIndex);
    }

    public void addPet(int familyIndex, Pet pet) {
        familyService.addPet(familyIndex, pet);
    }

    public void loadData (List <Family> families) {
        familyService.loadData(families);
    }
}