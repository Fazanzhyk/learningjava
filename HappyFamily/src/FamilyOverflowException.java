package happyfamily;

/*
* Class FamilyOverflowException is for throw error to Console if Family App running wrong way. It serves to watch that each Family has limit
* of humans
*
* @version 1.0 28 Feb 2020
*
* @author  Igor Ivanov
*  */

public class FamilyOverflowException extends RuntimeException {

    private int number;

    public int getNumber() {
        return number;
    }

    public FamilyOverflowException (String message, int number) {
        super(message);
        this.number = number;
    }
}