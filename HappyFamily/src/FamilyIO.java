package happyfamily;
import happyfamily.family.Family;
import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/*
 * Class Family IO is doing work of Input Output the families. It contains write and read methods.
 *
 * @version 1.1  02 Mar 2020
 *
 * @author  Igor Ivanov
 * */

public class FamilyIO {

    final String FAIL_MESSAGE = "Can't cast it, bro!";


    //your object need to implement Serializable if you want to IO it
    public void writeFamiliesIntoFile(List<Family> families, String fileName) throws IOException {
        // Open or create a new file
        FileOutputStream file = new FileOutputStream(fileName);
        // create an ObjOutStream giving FileOutStream as an arg to the constructor
        ObjectOutputStream objOut = new ObjectOutputStream(file);

        for (int i = 0; i < families.size(); i++) {
            objOut.writeObject(families.get(i));
        }

        objOut.close();
        file.close();
    }

    public ArrayList<Family> readFamiliesFromFile (String fileName) throws IOException, ClassNotFoundException, ParseException {

        FileInputStream file = new FileInputStream(fileName);
        ArrayList <Family> fams = new ArrayList<>();
        ObjectInputStream objIn = new ObjectInputStream(file);
        Family family;
        boolean cont = true;
        while (cont) {
            try {
                if (file.available() != 0) {
                    family = (Family) objIn.readObject();
                    fams.add(family);
                } else {
                    cont = false;
                }
            } catch (ClassNotFoundException | ClassCastException e) {
                System.out.println(FAIL_MESSAGE + ": " + e);
            }
        }
        objIn.close();
        file.close();
        return fams;
    }
}