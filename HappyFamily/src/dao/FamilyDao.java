package happyfamily.dao;

/*
* Interface FamilyDao provides ability to work with FamilyDataBase. It describes all methods, which are in need to work with DataBase.
*
* @version  2.1  02 Mar 2020
*
* @author   Igor Ivanov
*
* */

import happyfamily.family.Family;

import java.util.List;

public interface FamilyDao  {

    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    boolean deleteFamily(int index);

    boolean deleteFamily(Family family);

    void saveFamily (Family family);

    void setFamilyList (List <Family> families);
}