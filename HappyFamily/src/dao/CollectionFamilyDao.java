package happyfamily.dao;
import happyfamily.family.Family;

import java.util.*;

/*
*  Abstract class CollectionFamilyDao contains familyList, which is FamilyDataBase and implements following methods of FamilyDao interface
* to work with DataBase.
*
* @version 3.1  02 Mar 2020
*
* @author  Igor Ivanov
*
* */

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> familyList = new ArrayList<>();

    /*   getAllFamilies - returns list of families */

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }

   /*  getFamilyByIndex takes index and returns family, if it didn't find returns null*/

    @Override
    public Family getFamilyByIndex(int index) {
        if (!(index > (familyList.size() - 1))) {
            return familyList.get(index);
        } else {
            return null;
        }
    }

  /* deleteFamily takes index and delete family from the familyList and returns true. If element by index didn't find returns null  */

    @Override
    public boolean deleteFamily(int index) {
        if (!(index > (familyList.size() - 1))) {
            familyList.remove(index);
            return true;
        } else {
            return false;
        }
    }

    /* deleteFamily takes family and delete family from the familyList and returns true. If element by index didn't find returns null  */

    @Override
    public boolean deleteFamily(Family family) {
        return familyList.remove(family);
    }

   /* saveFamily takes family and, if element exists and it equals to passed one, than passed will replace those, which exists, else -
   * passed elem will be added to the end of list
   **/

    @Override
    public void saveFamily(Family family) {
        if (familyList.contains(family)) {
            familyList.set(familyList.indexOf(family), family);
        } else {
            familyList.add(family);
        }
    }

    @Override
    public void setFamilyList(List<Family> families) {
        this.familyList = families;
    }
}