package happyfamily.human;

/*
 * Class Human provides ability to create instances of Human. It contains 7 fields, 6 final Strings, 3 methods, 2 constructors, and 4
 * overrided methods. Get 2 instances of Human. Added describeAge() method
 *
 * @version  8.2  02 Mar 2020
 * @author   Igor Ivanov
 *
 * */

import happyfamily.family.Family;
import happyfamily.pets.Pet;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Date;
import java.util.Map;

public class Human implements Serializable {

    private String name;
    private String surname;
    private long birthDate;
    private int iqLevel;
    private Map<String, String> schedule;
    private Family family;

    private static final long serialVersionUID = 2L;
    final String GREETING = "Hello, %s \n";
    final String DESCRIPTION = "I have the %s, it is %d, it is %s \n";
    final String FEED_PET = "Hmmm, let me feed %s \n";
    final String DUNNO_FEED = "I think, %s is not hungry \n";
    final String ANNOUNCEMENT = "New Object had been created of type Human!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";
    final String SMART_SAW = "%s, you are unacceptably wrong! \n";
    final String STUPID_SAW = "WOMAN: Ha-ha-ha! You are stupid!";
    final String SILENCE = "MAN: ... (says nothing) \n";
    final String ANGRY_ANSWER = "%s Go to hell, %s! \n";
    final String DATE_FORMATTED = "dd/MM/yyyy";

    public long getBirthDate() {
        return birthDate;
    }
    public String getName() {
        return name;
    }
    public String getSurname () {
        return surname;
    }
    public int getIqLevel() {
        return iqLevel;
    }
    public Map<String, String> getSchedule () {
        return schedule;
    }
    public Family getFamily () {
        return family;
    }

    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }
    public void setName (String name) {
        this.name = name;
    }
    public void setSurname (String surname) {
        this.surname = surname;
    }
    public void setIqLevel (int iqLevel) {
        this.iqLevel = iqLevel;
    }
    public void setSchedule (Map<String, String> schedule) {
        this.schedule = schedule;
    }
    public void setFamily (Family family) {
        this.family = family;
    }

    public String describeAge() {
        Timestamp time = new Timestamp(this.getBirthDate());
        LocalDateTime d = time.toLocalDateTime();
        LocalDate bDay = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
        LocalDate now = LocalDate.now();
        Period diff = Period.between(bDay, now);
        return "\nYEARS: " +
                diff.getYears() +
                "\nMONTHS: " +
                diff.getMonths() +
                "\nDAYS: " +
                diff.getDays();
    }

    public String getFormattedBirthDate() {
        Timestamp time = new Timestamp(this.getBirthDate());
        LocalDateTime t = time.toLocalDateTime();
        StringBuilder strBld = new StringBuilder();
        return String.valueOf(strBld
                .append(t.getDayOfMonth())
                .append("/")
                .append(t.getMonth().getValue())
                .append("/")
                .append(t.getYear()));
    }

    void greetPet () {
        for (Pet pet : family.getPet()) {
            System.out.printf(GREETING, pet.getNickname());
        }
    }

    public int countAge () {
        Timestamp time = new Timestamp(this.getBirthDate());
        LocalDateTime d = time.toLocalDateTime();
        LocalDate bDay = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
        LocalDate now = LocalDate.now();
        Period diff = Period.between(bDay, now);
        return diff.getYears();
    }

    public void describePet() {
        for (Pet pet : family.getPet()) {
            System.out.printf(DESCRIPTION, pet.getSpecies(), pet.getAge(), pet.describeTrickLevel());
        }
    }

    public boolean feedPet (boolean isTime) {
        if (isTime) {
            for (Pet pet : family.getPet()) {
                System.out.printf(FEED_PET, pet.getNickname());
            }
            return true;
        } else {
            for (Pet pet : family.getPet()) {
                System.out.printf(DUNNO_FEED, pet.getNickname());
            }
            return false;
        }
    }

    public String prettyFormat() {
        StringBuilder strBld = new StringBuilder();
        return String.valueOf(strBld
                .append("{name=")
                .append(this.getName())
                .append(", surname=")
                .append(this.getSurname())
                .append(", birthDate=")
                .append(this.getFormattedBirthDate())
                .append(", iq=")
                .append(this.getIqLevel())
                .append(", schedule=")
                .append(this.getSchedule())
                .append("},\n"));
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        System.out.println(ANNOUNCEMENT);
    }

    public Human (String name, String surname, String birthDate, int iqLevel, Map<String, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;
        SimpleDateFormat format = new SimpleDateFormat(DATE_FORMATTED);
        Date date = format.parse(birthDate);
        this.birthDate = date.getTime();
        this.iqLevel = iqLevel;
        this.schedule = schedule;
    }

    @Override
    public String toString() {

        return this.getClass().getName().substring(18) +  " { name = "+getName()+", surname = "+getSurname()+", year = "+ this.countAge()+
                ", iq = "+getIqLevel()+", schedule = " + getSchedule();
    }
    @Override
    public int hashCode () {
        return name.hashCode() * 12 + surname.hashCode() * 22;
    }

    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Human)) {
            return false;
        }
        return this.hashCode() == obj.hashCode() && this.name.equals(((Human)obj).name)
                && this.surname.equals(((Human)obj).surname)
                && this.family==((Human)obj).family;
    }

    @Override
    public void finalize () {
        System.out.printf("Deleted obj: %s, has hashCode: %d, consists of: name: %s, surname: %s",
                this.getClass().getName(), this.hashCode(), this.name, this.surname);
    }
}
