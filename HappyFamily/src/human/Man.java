package happyfamily.human;

/*
 * Class Man is instance of Human. Overrides greetPet() and have own method silence()
 *
 * @version  2.1  26 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import happyfamily.pets.Pet;

import java.text.ParseException;
import java.util.Map;

public final class Man extends Human {

    final String MAN_GREETING = "Hi buddy, %s. How are you doing, pall? \n";

    public Man(String name, String surname, String birthDate, int iqLevel, Map<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iqLevel, schedule);
    }

    public void silence () {
        if (this.getIqLevel() > this.getFamily().getMother().getIqLevel()) {
            System.out.print(SILENCE);
        } else {
            System.out.printf(ANGRY_ANSWER, STUPID_SAW, getFamily().getMother().getName());
        }
    }

    @Override
    public void greetPet() {
        for (Pet pet : getFamily().getPet()) {
            System.out.printf(MAN_GREETING, pet.getNickname());
        }
    }
}
