package happyfamily.human;

/*
 * Class Woman is instance of Human. Overrides greetPet() and have own method saw()
 *
 * @version  2.1  26 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import happyfamily.pets.Pet;

import java.text.ParseException;
import java.util.Map;

public final class Woman extends Human {

    final String WOMAN_GREETING = "Hi, sweet little %s. I'm your mummy \n";

    public Woman(String name, String surname, String birthDate, int iqLevel, Map<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iqLevel, schedule);
    }

    public void saw () {
        if (this.getIqLevel() > this.getFamily().getFather().getIqLevel()) {
            System.out.printf(SMART_SAW, this.getFamily().getFather().getName());
        } else {
            System.out.println(STUPID_SAW);
        }
    }

    @Override
    public void greetPet() {
        for (Pet pet : getFamily().getPet()) {
            System.out.printf(WOMAN_GREETING, pet.getNickname());
        }
    }
}
