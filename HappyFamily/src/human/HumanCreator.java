import java.util.Random;

/*
 * Interface HumanCreator provides ability to create instances of Human. It contains default method bornChild()
 *
 * @version  4.0  27 Feb 2020
 * @author   Igor Ivanov
 *
 * */

public interface HumanCreator {

    String [] boyNames = {"John", "Bob", "Tony", "Gianni", "Gigi", "Nicolo", "Igor", "Maxim", "Dommy", "Sasha", "Orest", "Ostap", "Bran"};
    String [] girlNames = {"Oksana", "Bianka", "Yulia", "Alona", "Kathrin", "Ira", "Mariia", "Dominika", "Natasha", "Luisa", "Angelica"};

    default void bornChild (Family family) throws ParseException {
        Random random = new Random ();
        int randomSex = random.nextInt(101);
        LocalDate now = LocalDate.now();
        StringBuilder strBld = new StringBuilder();
        String birthDate = String.valueOf(strBld
                .append(now.getDayOfMonth())
                .append("/")
                .append(now.getMonth().getValue())
                .append("/")
                .append(now.getYear()));
        Human child;

        if (randomSex >= 50) {
            int randomBoyName = random.nextInt(boyNames.length);
            child = new Man (boyNames[randomBoyName], family.getFather().getSurname(), birthDate, family.getAverageIq(), null);
        } else {
            int randomGirlName = random.nextInt(girlNames.length);
            child = new Woman (girlNames[randomGirlName], family.getFather().getSurname(), birthDate, family.getAverageIq(), null);
        }

        family.setChildren(child);
    }
}
