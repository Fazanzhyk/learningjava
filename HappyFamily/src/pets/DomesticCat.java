package happyfamily.pets;

/*
 * Class DomesticCat is instance of Pet and implements interface Pets. Gets two methods respond and foul, which are overrided from Pet and Pets;
 * Has no own methods
 *
 * @version 2.0 12 Feb 2020
 *
 * @author  Igor Ivanov
 *
 * */

import java.util.Set;

public class DomesticCat extends Pet implements FoulingPet {

    final String DOMESTIC_CAT_RESPOND = "Hello, massa. I'm ... tired";

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.println(DOMESTIC_CAT_RESPOND);
    }

    @Override
    public void foul() {
        System.out.println(FOUL);
    }

}