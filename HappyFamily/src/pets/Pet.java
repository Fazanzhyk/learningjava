package happyfamily.pets;

/*
 * Abstract Class Pet provides ability to create instances of Pet. It contains 6 private fields, 7 final Strings to negotiate via Console,
 * 6 methods, 2 constructors, and 4 overrided methods. describeTrickLevel() returns final String (one of two specials) depending on trickLevel
 * passed via constructor, overrided hashCode() and equals(), toString() and finalize(). Have abstract mathod respond () and 4 instances of Pet;
 *
 * @version  5.2 02 Mar 2020
 * @author   Igor Ivanov
 *
 * */

import java.io.Serializable;
import java.util.Set;

import static happyfamily.pets.Species.UNKNOWN;

public abstract class Pet implements Serializable {

    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set <String> habits;

    private static final long serialVersionUID = 3L;
    final String EAT = "I'm eating";
    final String RESPOND = "Hello, master. I'm %s. I missed you!!! \n";
    final String FOUL = "I need to cover my tracks well...";
    final String TRICKY = "very tricky";
    final String NOT_TRICKY = "almost not tricky";
    final String ANNOUNCEMENT = "New Object had been created of type Pet!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";

    public Species getSpecies () {
        return species;
    }
    public String getNickname () {
        return nickname;
    }
    public int getAge () {
        return age;
    }
    public int getTrickLevel () {
        return trickLevel;
    }
    public Set<String> getHabits () {
        return habits;
    }

    public void setSpecies (Species species) {
        this.species = species;
    }
    public void setNickname (String nickname) {
        this.nickname = nickname;
    }
    public void setAge (int age) {
        this.age = age;
    }
    public void setTrickLevel (int level) {
        this.trickLevel = level;
    }
    public void setHabits (Set<String> args) {
        this.habits = args;
    }

    public void eat () {
        System.out.println(EAT);
    }

    abstract void respond ();

    public String describeTrickLevel () {
        if (getTrickLevel() > 50) {
            return TRICKY;
        } else {
            return NOT_TRICKY;
        }
    }

    public boolean isTimeToEat (int randomZeroOrOne , int random) {
        if (randomZeroOrOne == 0) {
            return trickLevel < random;
        } else {
            return true;
        }
    }

    public String prettyFormat() {
        StringBuilder strBld = new StringBuilder();
        return String.valueOf(strBld
                .append("{species=")
                .append(this.getSpecies())
                .append(", nickname=")
                .append(this.getNickname())
                .append(", age=")
                .append(this.getAge())
                .append(", trickLevel=")
                .append(this.getTrickLevel())
                .append(", habits=")
                .append(this.getHabits())
                .append("};\n"));
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        for (Species spec : Species.values()) {
            if (this.getClass().getSimpleName().toUpperCase().equals(spec.toString())) {
                this.setSpecies(spec);
                break;
            } else {
                this.setSpecies(UNKNOWN);
            }
        }
        System.out.println(ANNOUNCEMENT);
    }

    public Pet (String nickname) {
        this.nickname = nickname;
    }

    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this(nickname);
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    public String toString() {

        return getSpecies() + " { nickname = "+getNickname() +", age = "+getAge()+", trickLevel = "+getTrickLevel() +", habits = "
                + getHabits().toString() +", can fly = " + this.getSpecies().canFly +
                ", has number of legs = " + this.getSpecies().numberOfLegs + ", has fur = " + this.getSpecies().hasFur + " };";
    }
    @Override
    public int hashCode () {
        return nickname.hashCode() * 15 + getSpecies().hashCode() * 25;
    }

    @Override
    public boolean equals (Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Pet)) {
            return false;
        }
        return this.hashCode() == obj.hashCode() && this.species.equals(((Pet) obj).species)
                && this.nickname.equals(((Pet) obj).nickname);
    }

    @Override
    public void finalize () {
        System.out.printf("Deleted obj: %s, has hashCode: %d, of species: %s and nickname: %s",
                this.getClass().getName(), this.hashCode(), this.species.name(), this.nickname);
    }
}
