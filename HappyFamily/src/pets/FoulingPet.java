package happyfamily.pets;

/*
* Interface Pets implements method foul() for some instances of Pet;
*
* @version 2.0 12 Feb 2020
*
* @author  Igor Ivanov
*
* */

public interface FoulingPet {

    void foul ();

}
