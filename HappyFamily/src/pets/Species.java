package happyfamily.pets;

/*
* Simple enum Species
*
* */

public enum Species {
    DOG (false, 4, true),
    SNAKE (false, 0, false),
    RAT (false, 4, true),
    PARROT (true, 2, false),
    RABBIT (false, 4, true),
    SQUIRREL (true, 4, true),
    HORSE (false, 4, false),
    FISH (false, 0, false),
    ROBOCAT (false, 4, false),
    DOMESTICCAT (false, 4, true),
    UNKNOWN (false, 0, false);

    public boolean canFly;
    public int numberOfLegs;
    public boolean hasFur;

    Species (boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}
