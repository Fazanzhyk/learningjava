package happyfamily.pets;

/*
 * Class Dog is instance of Pet and implements interface Pets. Gets two methods respond and foul, which are overrided from Pet and Pets;
 * Has no own methods
 *
 * @version 2.0 12 Feb 2020
 *
 * @author  Igor Ivanov
 *
 * */

import java.util.Set;

public class Dog extends Pet implements FoulingPet {

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf(RESPOND, this.getNickname());
    }

    @Override
    public void foul() {
        System.out.println(FOUL);
    }
}
