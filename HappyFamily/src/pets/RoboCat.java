package happyfamily.pets;

/*
 * Class RoboCat is instance of Pet and implements interface Pets. Gets two methods respond and foul, which are overrided from Pet and Pets;
 * Has no own methods
 *
 * @version 2.0 12 Feb 2020
 *
 * @author  Igor Ivanov
 *
 * */

import java.util.Set;

public class RoboCat extends Pet {

    final String ROBO_CAT_RESPOND = "Hello, master. I'm robot, %s. I need your clothes, shoes and byke. And where Sarah Connor lives? \n";

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
    }

    @Override
    public void respond() {
        System.out.printf(ROBO_CAT_RESPOND, this.getNickname());
    }

}
