package happyfamily;

/*
* Simple Class Family Demo to run some code in Console
* */

import java.text.ParseException;

public class FamilyDemo {

    public static void main(String[] args) throws ParseException {

        FamilyApp familyApp = new FamilyApp();

        familyApp.familyAppRunner();
    }
}