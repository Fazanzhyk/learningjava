package happyfamily.family;

/*
 * Class Family provides ability to create instances of Family. It contains 5 private fields, 2 final Strings to negotiate via Console,
 * 3 methods (add/deleteChild & countFamily()), 1 constructor, and 4 overrided methods.
 *
 * @version  5.1  29 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import happyfamily.human.Human;
import happyfamily.human.HumanCreator;
import happyfamily.pets.Pet;

import java.io.Serializable;
import java.util.*;

public class Family implements HumanCreator, Serializable {

    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets;

    private static final long serialVersionUID = 1L;
    final String ANNOUNCEMENT = "New Object had been created of type Family!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";

    public Human getMother () {
        return mother;
    }
    public Human getFather () {
        return father;
    }
    public List<Human> getChildren () {
        return children;
    }
    public Set<Pet> getPet () {
        return pets;
    }

    public void setMother (Human mother) {
        this.mother = mother;
    }
    public void setFather (Human father) {
        this.father = father;
    }
    public void setChildren (Human ... children) {
        this.children.addAll(Arrays.asList(children));
    }
    public void setPet (Set<Pet> pets) {
        this.pets = pets;
    }

    public String prettyFormat () {
        StringBuilder strBld = new StringBuilder();
        strBld
                .append("\nFamily: \n")
                .append("mother: ")
                .append(this.getMother().prettyFormat())
                .append("father: ")
                .append(this.getFather().prettyFormat())
                .append("children:\n");

        if (getChildren().size() != 0) {
            for (int i = 0; i < getChildren().size(); i++) {
                strBld.append(getChildren().get(i).prettyFormat());
            }
        } else {
            strBld.append("This family has no children yet \n");
        }

        strBld.append("pets:\n");

        if (getPet().size() != 0) {
            for (Pet pet : getPet()) {
                strBld.append(pet.prettyFormat());
            }
        } else {
            strBld.append("This family has no pet");
        }

        return String.valueOf(strBld);
    }


    public int getAverageIq () {
        return ((father.getIqLevel() + mother.getIqLevel()) / 2);
    }

    public int countFamily () {
        return 2 + getChildren().size();
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        System.out.println(ANNOUNCEMENT);
    }

    public Family (Human mother, Human father, Set<Pet> pet, Human ...args) {
            setMother(mother);
            setFather(father);
            setPet(pet);
            setChildren(args);
            mother.setFamily(this);
            father.setFamily(this);
        for (Human arg : args) {
            arg.setFamily(this);
        }
    }

    @Override
    public String toString() {
        return this.getClass().getName() + "Mother: "+mother.toString()+" Father: "+father.toString()+" Children: "+ children.toString() +" Pets: "
                + (pets != null ? pets.toString(): "Undefined");
    }

    @Override
    public int hashCode() {
        return mother.hashCode() + father.hashCode();
    }

    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Family)) {
            return false;
        }
        return this.hashCode() == obj.hashCode() && this.mother==((Family) obj).mother
                && this.father==((Family) obj).father;
    }

    @Override
    public void finalize () {
        System.out.printf("Deleted obj: %s, has hashCode: %d", this.getClass().getName(), this.hashCode());
    }
}
