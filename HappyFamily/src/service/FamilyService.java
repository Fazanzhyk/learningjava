package happyfamily.sevice;
import happyfamily.FamilyOverflowException;
import happyfamily.dao.CollectionFamilyDao;
import happyfamily.dao.FamilyDao;
import happyfamily.family.Family;
import happyfamily.human.Human;
import happyfamily.pets.Pet;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/*
* This class provides ability to make some side effects with familyList Collection. Updated to version 2.0
*
* @version  4.0  02 Mar 2020
*
* @author   Igor Ivanov
* */

public class FamilyService {

    private FamilyDao familyDao;

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public FamilyService () {
        this.familyDao = new CollectionFamilyDao();
    }

    /* displayAllFamilies() display in Console All Families. Need to be passed to System.out.ptintln() method */
    public void displayAllFamilies() {
        StringBuilder str = new StringBuilder();
        familyDao.getAllFamilies()
                .forEach(elem -> System.out.println(str
            .append(familyDao.getAllFamilies().indexOf(elem) + 1)
            .append(elem.prettyFormat())));
}

  /* getFamiliesBiggerThan() display in Console all Families which suite for condition passed. */
    public List<Family> getFamiliesBiggerThan(int number) {
         return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > number)
                .collect(Collectors.toList());
    }

    /* getFamiliesLessThan() display in Console all Families which suite for condition passed */
    public List<Family> getFamiliesLessThan(int number) {
        return familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < number)
                .collect(Collectors.toList());
    }

    /* countFamiliesWithMemberNumber() returns number of families which are suite for condition passed */
    public long countFamiliesWithMemberNumber(int number) {
        return familyDao.getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == number)
                .count();
    }

    /* createNewFamily() creates new Family and saves it to familyList */
    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father, null);
        familyDao.saveFamily(family);
    }

    /* bornChild() method adding new Human to children List in family, and upgades this family in familyList */
    public Family bornChild(Family family, String maleName, String femaleName) throws ParseException {
        Random random = new Random ();
        if (family.countFamily() >= 5)
            throw new FamilyOverflowException ("This family is overflow by humans", 6);
        int randomSex = random.nextInt(101);
        Human children;
        LocalDate now = LocalDate.now();
        StringBuilder strBld = new StringBuilder();
        String birthDate = String.valueOf(strBld
                .append(now.getDayOfMonth())
                .append("/")
                .append(now.getMonth().getValue())
                .append("/")
                .append(now.getYear()));
        if (randomSex <= 50) {
            children = new Human(maleName, family.getFather().getSurname(), birthDate, family.getAverageIq(), null);
        } else {
            children = new Human(femaleName, family.getFather().getSurname(), birthDate, family.getAverageIq(), null);
        }
        family.setChildren(children);
        familyDao.saveFamily(family);
        return family;
    }

    /* adoptChild() method adding new Human to children List in family, and upgades this family in familyList */
    public Family adoptChild(Family family, Human human) {
        if (family.countFamily() >= 5)
            throw new FamilyOverflowException("This family already overflow by humans!", 6);
        family.setChildren(human);
        familyDao.saveFamily(family);
        return family;
    }

    /* deleteAllChildrenOlderThen takes childrenAge and updates number of children in family */
    public void deleteAllChildrenOlderThen(int childrenAge) {
        familyDao.getAllFamilies()
                .forEach(family -> family
                        .getChildren()
                        .removeIf(children -> children.countAge() > childrenAge));
    }

    /*count() counts number of families in familyList*/
    public int count() {
        return familyDao.getAllFamilies().size();
    }

    /* getPets () returns Set of Pets of passed family via index */
    public Set<Pet> getPets(int familyIndex) {
        return familyDao.getAllFamilies()
                .get(familyIndex)
                .getPet();
    }

    /* addPet() adding new Pet to the Set of Pets of passed family via index */
    public void addPet(int familyIndex, Pet pet) {
        familyDao.getAllFamilies()
                .get(familyIndex)
                .getPet()
                .add(pet);
    }

    // loading list of families, not one by one
    public void loadData (List <Family> families) {
        familyDao.setFamilyList(families);
    }
}
