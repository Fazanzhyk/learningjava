package happyfamily;
import happyfamily.controller.FamilyController;
import happyfamily.human.Human;
import happyfamily.human.Man;
import happyfamily.human.Woman;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/*
* Class Family App is doing work of running the application. It contains Main Menu, Additional Menu, Family Controller to get methods
* from Family Service and Collection Family DAO, also has 14 constant Strings to configure the application environment. Main code is inside
* familyAppRunner() - this method running the application
*
* @version 1.1  02 Mar 2020
*
* @author  Igor Ivanov
* */

public class FamilyApp {

    private final Map<Integer, String> MAIN_MENU = new HashMap<>();
    private final Map<Integer, String> ADDITIONAL_MENU = new HashMap<>();
    private FamilyController familyController;

    public FamilyApp () {
        this.familyController = new FamilyController();
    }

    public Map<Integer, String> getMAIN_MENU() {
        return MAIN_MENU;
    }

    public Map<Integer, String> getADDITIONAL_MENU() {
        return ADDITIONAL_MENU;
    }

    final String GET_REQUIRE_NUMBER = "Please, enter required number";
    final String GET_REQUIRE_AGE = "Please, enter required children's age";
    final String GET_NAME = "Please, enter %s name\n";
    final String GET_SURNAME = "Please, enter %s surname\n";
    final String GET_BIRTH_YEAR = "Please, enter %s year of birth\n";
    final String GET_BIRTH_MONTH = "Please, enter %s month of birth\n";
    final String GET_BIRTH_DAY = "Please, enter %s day of birth\n";
    final String GET_IQ = "Please, enter %s IQ level\n";
    final String GET_FAMILY_ID = "Please, enter required Family ID";
    final String OUT_OF_APP = "exit";
    final String SORRY_BRO = "Sorry, bro, no available option in this Menu by this number!";
    final String BRO_BE_SMART = "Bro, you entered invalid number. Please, try again!";
    final String NOTHING_TO_DISPLAY = "Sorry, bro, nothing to display";
    final String GET_FILE_NAME = "Please, enter your required file name!";


    public void fulfillMainMenu () {
        MAIN_MENU.put(1, "Fulfil App with my data by reading file...");
        MAIN_MENU.put(2, "Display all Families");
        MAIN_MENU.put(3, "Display all families bigger than...");
        MAIN_MENU.put(4, "Display all families less than...");
        MAIN_MENU.put(5, "Count families where number of members equals to...");
        MAIN_MENU.put(6, "Create new Family");
        MAIN_MENU.put(7, "Delete Family by index");
        MAIN_MENU.put(8, "Refactor Family by index"); // Requires additional menu
        MAIN_MENU.put(9, "Delete all children older than...");
        MAIN_MENU.put(10, "Write info about all families into my file...");
    }

    public void fulfillAdditionalMenu () {
        ADDITIONAL_MENU.put(1, "Born child");
        ADDITIONAL_MENU.put(2, "Adopt child");
        ADDITIONAL_MENU.put(3, "Return to Main Menu");
    }

    public void displayMainMenuPrettyWay() {
        for (Map.Entry<Integer, String> li : getMAIN_MENU().entrySet()) {
            System.out.printf("- %d : %s\n", li.getKey(), li.getValue());
        }
    }

    public void displayAdditionalMenuPrettyWay() {
        for (Map.Entry<Integer, String> li : getADDITIONAL_MENU().entrySet()) {
            System.out.printf("- %d : %s\n", li.getKey(), li.getValue());
        }
    }

    public void familyAppRunner() throws ParseException {

        fulfillMainMenu();
        fulfillAdditionalMenu();
        Scanner scanner = new Scanner(System.in);
        String req;
        String req2;
        FamilyIO io = new FamilyIO();
//        ArrayList <Family> arr;

        do {
            displayMainMenuPrettyWay();

            req = scanner.next();

           if (!(req.equalsIgnoreCase(OUT_OF_APP))) {
               try {
                   if (Integer.parseInt(req) == 1) {
                       System.out.println(GET_FILE_NAME);
                       // read from file and add to data base
                       familyController.getFamilyService().getFamilyDao().setFamilyList(io.readFamiliesFromFile(scanner.next()));
                       System.out.println("Successfully: " + familyController.getFamilyService().getFamilyDao().getAllFamilies().size());
                   } else if (Integer.parseInt(req) == 2) {
                       if (familyController.getFamilyService().getFamilyDao().getAllFamilies().size() != 0)
                           familyController.displayAllFamilies();
                       else
                           System.out.println(NOTHING_TO_DISPLAY);
                   } else if (Integer.parseInt(req) == 3) {
                       System.out.println(GET_REQUIRE_NUMBER);
                       System.out.println(familyController.getFamiliesBiggerThan(Integer.parseInt(scanner.next())));
                   } else if (Integer.parseInt(req) == 4) {
                       System.out.println(GET_REQUIRE_NUMBER);
                       System.out.println(familyController.getFamiliesLessThan(Integer.parseInt(scanner.next())));
                   } else if (Integer.parseInt(req) == 5) {
                       System.out.println(GET_REQUIRE_NUMBER);
                       System.out.println(familyController.countFamiliesWithMemberNumber(Integer.parseInt(scanner.next())));
                   } else if (Integer.parseInt(req) == 6) {
                       System.out.printf(GET_NAME, "mother's");
                       String maName = scanner.next();
                       System.out.printf(GET_SURNAME, "mother's");
                       String maSurname = scanner.next();
                       System.out.printf(GET_BIRTH_DAY, "mother's");
                       String maDay = scanner.next();
                       System.out.printf(GET_BIRTH_MONTH, "mother's");
                       String maMonth = scanner.next();
                       System.out.printf(GET_BIRTH_YEAR, "mother's");
                       String maYear = scanner.next();
                       String maBirthDay = String.join("/", maDay, maMonth, maYear);
                       System.out.printf(GET_IQ, "mother's");
                       int maIqLevel = Integer.parseInt(scanner.next());
                       Woman ma = new Woman(maName, maSurname, maBirthDay, maIqLevel, new HashMap<>());

                       System.out.printf(GET_NAME, "father's");
                       String faName = scanner.next();
                       System.out.printf(GET_SURNAME, "father's");
                       String faSurname = scanner.next();
                       System.out.printf(GET_BIRTH_DAY, "father's");
                       String faDay = scanner.next();
                       System.out.printf(GET_BIRTH_MONTH, "father's");
                       String faMonth = scanner.next();
                       System.out.printf(GET_BIRTH_YEAR, "father's");
                       String faYear = scanner.next();
                       String faBirthDay = String.join("/", faDay, faMonth, faYear);
                       System.out.printf(GET_IQ, "father's");
                       int faIqLevel = Integer.parseInt(scanner.next());
                       Man pa = new Man(faName, faSurname, faBirthDay, faIqLevel, new HashMap<>());

                       familyController.createNewFamily(ma, pa);
                       System.out.println("Success!!!" + familyController.getFamilyService().getFamilyDao().getAllFamilies().size());
                   } else if (Integer.parseInt(req) == 7) {
                       System.out.println(GET_FAMILY_ID);
                       System.out.println(familyController.getFamilyService().getFamilyDao().deleteFamily(Integer.parseInt(scanner.next())));
                   } else if (Integer.parseInt(req) == 8) {

                       do {
                           displayAdditionalMenuPrettyWay();

                           req2 = scanner.next();

                           if (Integer.parseInt(req2) == 1) {
                               System.out.println(GET_FAMILY_ID);
                               int familyId = Integer.parseInt(scanner.next());
                               System.out.printf(GET_NAME, "boy's");
                               String boyName = scanner.next();
                               System.out.printf(GET_NAME, "girl's");
                               String girlName = scanner.next();
                               System.out.println(familyController
                                       .bornChild(familyController.getFamilyService().getFamilyDao().getFamilyByIndex(familyId - 1),
                                               boyName, girlName)
                                       .prettyFormat());
                           } else if (Integer.parseInt(req2) == 2) {
                               System.out.println(GET_FAMILY_ID);
                               int family1Id = Integer.parseInt(scanner.next());
                               System.out.printf(GET_NAME, "child's");
                               String name = scanner.next();
                               System.out.printf(GET_SURNAME, "child's");
                               String surname = scanner.next();
                               System.out.printf(GET_BIRTH_DAY, "child's");
                               String day = scanner.next();
                               System.out.printf(GET_BIRTH_MONTH, "child's");
                               String month = scanner.next();
                               System.out.printf(GET_BIRTH_YEAR, "child's");
                               String year = scanner.next();
                               String adoptChildBirthDay = String.join("/", day, month, year);
                               System.out.printf(GET_IQ, "child's");
                               int iq = Integer.parseInt(scanner.next());
                               Human human = new Human(name, surname, adoptChildBirthDay, iq, new HashMap<>());
                               System.out.println(familyController
                                       .adoptChild(familyController.getFamilyService().getFamilyDao().getFamilyByIndex(family1Id - 1), human)
                                       .prettyFormat());
                           } else {
                               System.out.println(SORRY_BRO);
                           }

                       } while (Integer.parseInt(req2) != 3 || !(req2.equalsIgnoreCase(OUT_OF_APP)));

                   } else if (Integer.parseInt(req) == 9) {
                       System.out.println(GET_REQUIRE_AGE);
                       familyController.deleteAllChildrenOlderThen(Integer.parseInt(scanner.next()));
                   } else if (Integer.parseInt(req) == 10) {
                       System.out.println(GET_FILE_NAME);
                       // put some logic to write into the file here
                       io.writeFamiliesIntoFile(familyController.getFamilyService().getFamilyDao().getAllFamilies(), scanner.next());
                   } else {
                       System.out.println(SORRY_BRO);
                   }
               } catch (NumberFormatException | IOException | ClassNotFoundException ex) {
                   System.out.println(BRO_BE_SMART + ": " + ex);
               }
           }
        } while (!(req.equalsIgnoreCase(OUT_OF_APP)));
    }
}