package happyfamily.test;

import happyfamily.human.Human;
import happyfamily.human.Man;
import happyfamily.human.Woman;
import org.junit.Test;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import static org.junit.jupiter.api.Assertions.*;

public class OfHumanTypeTest {

    Map<String, String> schedule = new HashMap<>();
    Man man = new Man ("Gianni", "Ventura", "01/11/1969", 169, schedule);
    Woman woman = new Woman ("Tina", "Kandelaki", "05/06/1990", 179, schedule);

    public OfHumanTypeTest() throws ParseException {
    }

    @Test
    public void twoStringsForHumanShouldBeEquals() throws Exception {

        schedule.put("Today", "I'm going to do nothing");
        Human human = new Human("Igor", "Ivanov", "16/10/1989", 200, schedule);
        String str = human.toString();
        String str2 = "Human { name = Igor, surname = Ivanov, year = 30, iq = 200, schedule = {Today=I'm going to do nothing}";
        assertEquals(str2, str);
    }

    @Test
    public void testTwoStringForManShouldBeEquals() throws Exception {

        schedule.put("Tuesday", "Kill Billy Letucci");
        String str = man.toString();
        String str1 = "Man { name = Gianni, surname = Ventura, year = 50, iq = 169, schedule = {Tuesday=Kill Billy Letucci}";
        assertEquals(str1, str);
    }

    @Test
    public void testTwoStringForWomanShouldBeEquals () throws Exception {

        schedule.put("Thursday", "TV Show Very Smart");
        String str = woman.toString();
        String str1 = "Woman { name = Tina, surname = Kandelaki, year = 29, iq = 179, schedule = {Thursday=TV Show Very Smart}";
        assertEquals(str1, str);
    }
}