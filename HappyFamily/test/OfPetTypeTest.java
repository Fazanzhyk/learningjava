package happyfamily.test;

import happyfamily.pets.Dog;
import happyfamily.pets.DomesticCat;
import happyfamily.pets.Fish;
import happyfamily.pets.RoboCat;
import org.junit.Test;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.*;

public class OfPetTypeTest {

    Set<String> habits = new HashSet<>();
    Dog dog = new Dog("Sharik", 2, 56, habits);
    DomesticCat cat = new DomesticCat("Murzik", 3, 99, habits);
    RoboCat robo = new RoboCat("Meow-222", 250, 100, habits);
    Fish fish = new Fish ("Fishy", 1, 1, habits);

    public void setHabits (String ...args) {
        habits.addAll(Arrays.asList(args));
    }

    @Test
    public void twoStringsForDogShouldBeEquals () throws Exception {
        setHabits("eat", "sleep", "howl");
        String str = dog.toString();
        String str1 = "DOG { nickname = Sharik, age = 2, trickLevel = 56, habits = [sleep, eat, howl], can fly = false, " +
                "has number of legs = 4, has fur = true };";
        assertEquals(str1, str);
    }

    @Test
    public void twoStringsForDomesticCatShouldBeEquals () throws Exception {
        setHabits("eat", "meowl", "sleep");
        String catStr = cat.toString();
        String str2 = "DOMESTIC_CAT { nickname = Murzik, age = 3, trickLevel = 99, habits = [sleep, meowl, eat], " +
                "can fly = false, has number of legs = 4, has fur = true };";
        assertEquals(str2, catStr);
    }

    @Test
    public void twoStringsForFishShouldBeEquals () throws Exception {
        setHabits("swim", "bubble");
        String fishStr = fish.toString();
        String str4 = "FISH { nickname = Fishy, age = 1, trickLevel = 1, habits = [bubble, swim], can fly = false, has number of legs = 0, " +
                "has fur = false };";
        assertEquals(str4, fishStr);
    }

    @Test
    public void twoStringsForRoboCopShouldBeEquals() throws Exception {
        setHabits("find Sarah Connor", "take shoes, clothes and byke");
        String roboStr = robo.toString();
        String str3 = "ROBO_CAT { nickname = Meow-222, age = 250, trickLevel = 100, habits = [take shoes, clothes and byke, find Sarah Connor], " +
                "can fly = false, has number of legs = 4, has fur = false };";
        assertEquals(str3, roboStr);
    }
}
