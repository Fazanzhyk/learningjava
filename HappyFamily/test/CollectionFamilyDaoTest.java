package happyfamily.family;

import happyfamily.human.Human;
import happyfamily.human.Man;
import happyfamily.human.Woman;
import happyfamily.pets.*;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class CollectionFamilyDaoTest {

    // Set for gangster family
    Map<String, String> schedule = new HashMap<>();
    Set<String> habits = new HashSet<>();
    Human bianka = new Human("Bianka", "Rataikovski", "12/01/1989", 56, schedule);
    Human billy = new Human ("Billy", "Belucci", "25/12/1988", 99, schedule);
    Pet dog = new Dog("Sharik", 8, 99, habits);
    Pet robo = new RoboCat("Robik", 99, 100, habits);
    Set<Pet> gangstaPet = new HashSet<>(Arrays.asList(dog, robo));
    Human aleks = new Human ("Aleks", "Belucci", "26/10/2010", 0, schedule);
    Human aleksa = new Human ("Aleksa", "Belucci", "13/08/2011", 0, schedule);

    //Set for royal family
    Human kathrin = new Woman("Kathrin", "Middleton", "01/02/1982", 250, schedule);
    Human charles = new Man("Charles", "Prince", "31/03/1981", 200, schedule);
    Pet cat = new DomesticCat("England", 1, 88, habits);
    Pet fish = new Fish("Scotland", 2, 1, habits);
    Set<Pet> royalPet = new HashSet<>(Arrays.asList(cat, fish));
    Human vika = new Human("Viktoriya", "Princess", "17/04/2015", 5, schedule);

    // Set for David Beckham Family
    Human viktoriya = new Woman("Vika", "Adams", "03/05/1975", 55, schedule);
    Human david = new Man ("David", "Beckham", "06/06/1977", 69, schedule);
    Pet doggy = new Dog ("Fergy", 6, 55, habits);
    Pet kitty = new DomesticCat("Scolzy", 9, 66, habits);
    Pet rayan = new Fish ("Giggzy", 1, 6, habits);
    Set<Pet> backhamPet = new HashSet<>(Arrays.asList(doggy,kitty,rayan));
    Human romeo = new Human ("Romeo", "Backham", "20/01/2002", 2, schedule);
    Human brooklyn = new Human ("Brooklyn", "Backham", "18/02/1999", 3, schedule);
    Human kruz = new Human ("Kruz", "Backham", "30/08/2005", 4, schedule);

    // Set for pop-star family
    Human alla = new Woman ("Alla", "Pugachova", "09/05/1955", 144, schedule);
    Human maksym = new Man ("Maksym", "Galkin", "01/05/1985", 95, schedule);
    Pet sharik = new Dog("Sharik", 7, 69, habits);
    Set<Pet> popStarPet = new HashSet<>(Collections.singletonList(sharik));

    // Set for another family
    Human igor = new Man ("Igor", "Ivanov", "16/10/1989", 250, schedule);
    Human oksana = new Woman("Oksana", "Musiienko", "03/09/1989", 199, schedule);
    Human khrystya = new Human ("Krystyna", "Musiienko", "08/05/2018", 250, schedule);

    // Set for modern family
    Human petya = new Man ("Petya", "Pavlov", "13/07/1995", 220, schedule);
    Human vasya = new Man ("Vasya", "Shatalov", "06/02/1997", 210, schedule);


    Family gangsterFamily = new Family(bianka, billy, gangstaPet, aleks, aleksa);
    Family royalFamily = new Family (kathrin, charles, royalPet, vika);
    Family backhamFamily = new Family(viktoriya, david, backhamPet, romeo, brooklyn, kruz);
    Family popStarFamily = new Family(alla, maksym, popStarPet);
    Family myFamily = new Family(oksana, igor, null, khrystya);
    Family modernFamily = new Family(petya, vasya, null);

    FamilyController familyController = new FamilyController();

    CollectionFamilyDaoTest() throws ParseException {
    }


    void fulfillFamilyDataBaseForCollectionDaoTest () throws Exception {
        familyController.getFamilyService().getFamilyDao().saveFamily(gangsterFamily);
        familyController.getFamilyService().getFamilyDao().saveFamily(royalFamily);
        familyController.getFamilyService().getFamilyDao().saveFamily(backhamFamily);
        familyController.getFamilyService().getFamilyDao().saveFamily(popStarFamily);
        familyController.getFamilyService().getFamilyDao().saveFamily(myFamily);
        familyController.getFamilyService().getFamilyDao().saveFamily(modernFamily);
    }

    @Test
    void getFamilyByIndex() throws Exception {
        fulfillFamilyDataBaseForCollectionDaoTest();
        assertSame(familyController.getFamilyService().getFamilyDao().getFamilyByIndex(0), gangsterFamily);
        assertSame(familyController.getFamilyService().getFamilyDao().getFamilyByIndex(3), popStarFamily);
        assertSame(familyController.getFamilyService().getFamilyDao().getFamilyByIndex(5), modernFamily);
        assertNull(familyController.getFamilyService().getFamilyDao().getFamilyByIndex(6));
    }

    @Test
    void deleteFamily() throws Exception {
        fulfillFamilyDataBaseForCollectionDaoTest();
        assertTrue(familyController.getFamilyService().getFamilyDao().deleteFamily(0));
        assertTrue(familyController.getFamilyService().getFamilyDao().deleteFamily(3));
        assertFalse(familyController.getFamilyService().getFamilyDao().deleteFamily(5));
    }

    @Test
    void testDeleteFamily() throws Exception {
        fulfillFamilyDataBaseForCollectionDaoTest();
        Family newFamily = new Family(new Human ("Olga", "Sharij", "22/02/1988", 99, schedule),
                new Human ("Anatolii", "Sharij", "12/05/1986", 101, schedule), gangstaPet);
        assertTrue(familyController.getFamilyService().getFamilyDao().deleteFamily(gangsterFamily));
        assertTrue(familyController.getFamilyService().getFamilyDao().deleteFamily(myFamily));
        assertFalse(familyController.getFamilyService().getFamilyDao().deleteFamily(newFamily));
    }

    @Test
    void saveFamily() throws Exception {
        fulfillFamilyDataBaseForCollectionDaoTest();
        Family newFamily = new Family(new Human ("Olga", "Sharij", "22/02/1988", 99, schedule),
                new Human ("Anatolii", "Sharij", "12/05/1986", 101, schedule), gangstaPet);
        familyController.getFamilyService().getFamilyDao().saveFamily(newFamily);
        assertTrue(familyController.getFamilyService().count() == 7);
        familyController.getFamilyService().getFamilyDao().saveFamily(backhamFamily);
        assertTrue(familyController.getFamilyService().count() == 7);
    }
}