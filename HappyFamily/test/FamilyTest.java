package happyfamily.test;

import happyfamily.family.Family;
import happyfamily.human.Human;
import happyfamily.pets.Pet;
import happyfamily.pets.RoboCat;
import org.junit.Test;

import java.text.ParseException;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

public class FamilyTest {

    Map<String, String> schedule = new HashMap<>();
    Set<String> habits = new HashSet<>();
    Human tony = new Human ("Tony" , "Chekhov", "16/10/1989", 199, schedule);
    Human bianka = new Human("Bianka", "Dedovic", "17/05/1999", 122, schedule);
    Human tracy = new Human ("Trace", "Chekhov", "05/09/2019", 56, schedule);
    Human billy = new Human ("Billy", "Dedovic", "01/01/2020", 69, schedule);
    RoboCat bob = new RoboCat("Bob", 5, 56, habits);
    Set<Pet> pets = new HashSet<>();

    public FamilyTest() throws ParseException {
    }


    @Test
    public void twoStringsForFamilyShouldBeEquals () throws Exception {
        schedule.put("Monday", "Go to Dan IT");
        habits.add("get byke, clothes");
        pets.add(bob);
        Family family = new Family(bianka, tony, pets, tracy, billy);
        String str = family.toString();
        String str2 = "Mother: Human { name = Bianka, surname = Dedovic, year = 20, iq = 122, schedule = {Monday=Go to Dan IT} " +
                "Father: Human { name = Tony, surname = Chekhov, year = 30, iq = 199, schedule = {Monday=Go to Dan IT} " +
                "Children: [Human { name = Trace, surname = Chekhov, year = 0, iq = 56, schedule = {Monday=Go to Dan IT}, " +
                "Human { name = Billy, surname = Dedovic, year = 0, iq = 69, schedule = {Monday=Go to Dan IT}] " +
                "Pets: [ROBOCAT { nickname = Bob, age = 5, trickLevel = 56, habits = [get byke, clothes], can fly = false, " +
                "has number of legs = 4, has fur = false };]";

        assertEquals(str2, str);
    }

    @Test
    public void testIfPassedFourMembersShouldReturnCountFour () throws Exception {
        pets.add(bob);
        Family family = new Family(bianka, tony, pets, tracy, billy);
        assertEquals(4, family.countFamily());
    }
}

