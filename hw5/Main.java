package hw5;

/*
* This is simple runner Class - via which the app runs. Method equals() overrided in that logic, that every objects, which belong to the
* same Family, should be equal between each other, no matter if it is Human or Pet - all equals inside one Family, since it wasn't
* obviously described in technical task
*
* @version 2.0 2 Feb 2020
* @author  Igor Ivanov
* */

import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Human alla = new Human ("Alla", "Pugacheva", 1949);
        Human filip = new Human ("Filip", "Kirkorov", 1960);
        Human kris = new Human ("Krystina", "Orbakayte", 1971);
        Human maxim = new Human ("Maxim", "Galkin", 1982);
        Pet sharik = new Pet ("dog", "Sharik", 7, 44, new String[]{"barkle", "howl", "sleep"});

        Family arlekinoFamily = new Family(alla, filip, sharik, kris, maxim);

        Family arlekinoFamily2 = arlekinoFamily;

        System.out.println(alla.hashCode());  // 3924
        System.out.println(sharik.hashCode()); // 18
        System.out.println(filip.hashCode()); // 3946
        System.out.println(kris.hashCode()); // 3976
        System.out.println(maxim.hashCode()); // 3986
        System.out.println(arlekinoFamily.hashCode()); // 15850
        System.out.println(arlekinoFamily.equals(arlekinoFamily2));  // true

    }
}

//    for future updates (for myself)
//    В Family опишите метод удалить ребенка(deleteChild) (принимает тип Human и удаляет соответствующий элемент). Метод должен быть написан
//        с учетом наличия методов equals() и hashCode().
//        Подсказка: для того, чтобы удалить верный элемент из массива Human'ов вам необходимо делать сравнения по полям идентифицирующим
//        именно данного человека (подумайте, какие поля для этого подходят).