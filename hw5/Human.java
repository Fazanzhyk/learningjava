package hw5;

/*
 * Class Human provides ability to create instances of Human. It contains 7 fields, 6 final Strings, 3 methods, 2 constructors, and 3
 * overrided methods.
 *
 * @version  2.1  2 Feb 2020
 * @author   Igor Ivanov
 *
 * */

import java.util.Arrays;

public class Human {

    private String name;
    private String surname;
    private int birthYear;
    private int iqLevel;
    private String [][] schedule;
    private Family family;

    final String GREETING = "Hello, %s \n";
    final String DESCRIPTION = "I have the %s, it is %d, it is %s \n";
    final String FEED_PET = "Hmmm, let me feed %s \n";
    final String DUNNO_FEED = "I think, %s is not hungry \n";
    final String ANNOUNCEMENT = "New Object had been created of type Human!";
    final static String UPLOADING = "Class Family is uploading! Please, wait a bit!";

    public String getName() {
        return name;
    }
    public String getSurname () {
        return surname;
    }
    public int getBirthYear () {
        return birthYear;
    }
    public int getIqLevel() {
        return iqLevel;
    }
    public String [][] getSchedule () {
        return schedule;
    }
    public Family getFamily () {
        return family;
    }

    public void setName (String name) {
        this.name = name;
    }
    public void setSurname (String surname) {
        this.surname = surname;
    }
    public void setBirthYear (int birthYear) {
        this.birthYear = birthYear;
    }
    public void setIqLevel (int iqLevel) {
        this.iqLevel = iqLevel;
    }
    public void setSchedule (String [][] schedule) {
        this.schedule = schedule;
    }
    public void setFamily (Family family) {
        this.family = family;
    }

    void greetPet () {
        System.out.printf(GREETING, family.getPet().getNickname());
    }

    void describePet () {
        System.out.printf(DESCRIPTION, family.getPet().getSpecies(), family.getPet().getAge(), family.getPet().describeTrickLevel());
    }

    public boolean feedPet (boolean isTime) {
        if (isTime) {
            System.out.printf(FEED_PET, family.getPet().getNickname());
            return true;
        } else {
            System.out.printf(DUNNO_FEED, family.getPet().getNickname());
            return false;
        }
    }

    static {
        System.out.println(UPLOADING);
    }

    {
        System.out.println(ANNOUNCEMENT);
    }

    Human (String name, String surname, int birthYear) {
        this.name = name;
        this.surname = surname;
        this.birthYear = birthYear;
    }

    Human (String name, String surname, int birthYear, int iqLevel, String[][] schedule) {
        this(name, surname, birthYear);
        this.iqLevel = iqLevel;
        this.schedule = schedule;

    }

    @Override
    public String toString() {
        return "Human { name = "+getName()+", surname = "+getSurname()+", year = "+getBirthYear()+", iq = "+getIqLevel()+", schedule = "
                + Arrays.deepToString(getSchedule());
    }
    @Override
    public int hashCode () {
        return name.length() * 2 + surname.length() * 2 + birthYear * 2;
    }
    @Override
    public boolean equals (Object obj) {
        if (!(obj instanceof Human)) {
            return false;
        }
        return this.hashCode() == obj.hashCode() && this.name.equals(((Human)obj).name)
                && this.surname.equals(((Human)obj).surname) && this.birthYear == ((Human)obj).birthYear
                && this.family==((Human)obj).family;
    }
}
